import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDTO } from './create-user.dto';
import { User } from "./user.interface";

@Injectable()
export class UsersService {
  constructor(@InjectModel('Users') private readonly usersModel: Model<User>) {}

  async create(createUserDto: CreateUserDTO): Promise<User> {
    const createUser = new this.usersModel(createUserDto);
    return createUser.save();
  }

  async findAll(): Promise<any[]> {
    return this.usersModel.find().exec();
  }
}