export interface User {
    name: string;
    cellPhone: string;
    email: string;
}