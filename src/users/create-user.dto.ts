export class CreateUserDTO {
    name: string;
    cellPhone: string;
    email: string
}