import { Controller, Post, Body, Get } from '@nestjs/common';
import { CreateUserDTO } from './create-user.dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {

    constructor(private userService: UsersService) {}

    @Get()
    sayHello() {
        return "HELLO";
    }

    @Post()
    async create(@Body() createUserDTO: CreateUserDTO) {
        this.userService.create(createUserDTO);
    }

}
