import * as mongoose from "mongoose";

export const UserSchema = new mongoose.Schema({
    name: String,
    cellPhone: String,
    email: String
});